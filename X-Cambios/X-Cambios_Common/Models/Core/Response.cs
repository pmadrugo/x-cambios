﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace X_Cambios_Common.Models.Core
{
    public class Response
    {
        public bool IsSuccess { get; set; }

        public string Message { get; set; }

        [JsonProperty(Required = Required.Default)]
        
        public object Result { get; set; }
    }
}
