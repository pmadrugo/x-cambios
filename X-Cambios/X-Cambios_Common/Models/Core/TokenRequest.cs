﻿using System;
using System.Collections.Generic;
using System.Text;

namespace X_Cambios_Common.Models.Core
{
    public class TokenRequest
    {
        public string Username { get; set; }

        public string Password { get; set; }
    }
}
