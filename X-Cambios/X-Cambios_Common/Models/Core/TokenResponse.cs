﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace X_Cambios_Common.Models.Core
{
    public class TokenResponse
    {
        [JsonProperty(PropertyName = "token")]
        public string Token { get; set; }

        [JsonProperty(PropertyName = "expiration")]
        public DateTime Expiration { get; set; }
    }
}
