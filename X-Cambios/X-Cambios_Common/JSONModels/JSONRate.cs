﻿using System;
using System.Collections.Generic;
using System.Text;

namespace X_Cambios_Common.JSONModels
{
    public class JSONRate
    {
        public int RateId { get; set; }

        public string Code { get; set; }

        public double TaxRate { get; set; }

        public string Name { get; set; }
    }
}
