﻿using Plugin.Connectivity;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using X_Cambios_Common.Models.Core;

namespace X_Cambios_Common.Services
{
    public class NetService
    {
        public async Task<Response> CheckConnection()
        {
            if (!CrossConnectivity.Current.IsConnected)
            {
                return new Response()
                {
                    IsSuccess = false,
                    Message = "Enable your internet connection."
                };
            }

            var isReacheable = await CrossConnectivity.Current.IsRemoteReachable(
                "google.com");

            if (!isReacheable)
            {
                return new Response()
                {
                    IsSuccess = false,
                    Message = "Check if your internet connection is working properly."
                };
            }

            return new Response()
            {
                IsSuccess = true,
                Message = "Ok"
            };
        }
    }
}
