﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using X_Cambios_Common.Models.Core;
using System.Linq;
using System.Net.Http.Headers;

namespace X_Cambios_Common.Services
{
    public class APIService
    {
        /// <summary>
        /// Do not forget that it is List<T> if more than one object!
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="urlBase"></param>
        /// <param name="route"></param>
        /// <param name="tokenType"></param>
        /// <param name="accessToken"></param>
        /// <returns></returns>
        public async Task<Response> GetAsync<T>(
            string urlBase,
            string[] route,
            bool hasWrapper = true)
        {
            try
            {
                var client = new HttpClient()
                {
                    BaseAddress = new Uri(urlBase)
                };

                var url = $"/{string.Join("/", route)}";

                var response = await client.GetAsync(url);
                var result = await response.Content.ReadAsStringAsync();

                if (!response.IsSuccessStatusCode)
                {
                    return new Response()
                    {
                        IsSuccess = false,
                        Message = result
                    };
                }

                if (!hasWrapper)
                {
                    return new Response()
                    {
                        IsSuccess = true,
                        Message = string.Empty,
                        Result = JsonConvert.DeserializeObject<T>(result)
                    };
                }

                var deserializeResponse = JsonConvert.DeserializeObject<Response>(result);
                if (!deserializeResponse.IsSuccess)
                {
                    return deserializeResponse;
                }

                if (deserializeResponse.Result == null)
                {
                    return new Response()
                    {
                        IsSuccess = true,
                        Message = deserializeResponse.Message ?? string.Empty
                    };
                }

                return new Response()
                {
                    IsSuccess = true,
                    Message = deserializeResponse.Message ?? string.Empty,
                    Result = JsonConvert.DeserializeObject<T>(deserializeResponse.Result.ToString())
                };
            }
            catch (Exception ex)
            {
                return new Response()
                {
                    IsSuccess = false,
                    Message = ex.Message
                };
            }
        }

        /// <summary>
        /// Do not forget that it is List<T> if more than one object!
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="urlBase"></param>
        /// <param name="route"></param>
        /// <param name="tokenType"></param>
        /// <param name="accessToken"></param>
        /// <returns></returns>
        public async Task<Response> GetAsync<T>(
            string urlBase,
            string[] route,
            string tokenType,
            string accessToken)
        {
            try
            {
                var client = new HttpClient()
                {
                    BaseAddress = new Uri(urlBase)
                };

                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue(tokenType, accessToken);

                var url = $"/{string.Join("/", route)}";

                var response = await client.GetAsync(url);
                var result = await response.Content.ReadAsStringAsync();

                if (!response.IsSuccessStatusCode)
                {
                    return new Response()
                    {
                        IsSuccess = false,
                        Message = result
                    };
                }

                var deserializeResponse = JsonConvert.DeserializeObject<Response>(result);
                if (!deserializeResponse.IsSuccess)
                {
                    return deserializeResponse;
                }

                if (deserializeResponse.Result == null)
                {
                    return new Response()
                    {
                        IsSuccess = true,
                        Message = deserializeResponse.Message ?? string.Empty
                    };
                }

                return new Response()
                {
                    IsSuccess = true,
                    Message = deserializeResponse.Message ?? string.Empty,
                    Result = JsonConvert.DeserializeObject<T>(deserializeResponse.Result.ToString())
                };
            }
            catch (Exception ex)
            {
                return new Response()
                {
                    IsSuccess = false,
                    Message = ex.Message
                };
            }
        }

        public async Task<Response> PostAsync<T>(
            string urlBase,
            string[] route,
            object request)
        {
            try
            {
                var requestString = JsonConvert.SerializeObject(request);
                var content = new StringContent(requestString, Encoding.UTF8, "application/json");
                var client = new HttpClient()
                {
                    BaseAddress = new Uri(urlBase)
                };

                var url = $"/{string.Join("/", route)}";

                var response = await client.PostAsync(url, content);
                var result = await response.Content.ReadAsStringAsync();

                if (!response.IsSuccessStatusCode)
                {
                    return new Response()
                    {
                        IsSuccess = false,
                        Message = result
                    };
                }

                var deserializeResponse = JsonConvert.DeserializeObject<Response>(result);
                if (!deserializeResponse.IsSuccess)
                {
                    return deserializeResponse;
                }

                if (deserializeResponse.Result == null)
                {
                    return new Response()
                    {
                        IsSuccess = true,
                        Message = deserializeResponse.Message ?? string.Empty
                    };
                }

                return new Response()
                {
                    IsSuccess = true,
                    Message = deserializeResponse.Message ?? string.Empty,
                    Result = JsonConvert.DeserializeObject<T>(deserializeResponse.Result.ToString())
                };
            }
            catch (Exception ex)
            {
                return new Response()
                {
                    IsSuccess = false,
                    Message = ex.Message
                };
            }
        }

        public async Task<Response> PostAsync<T>(
            string urlBase,
            string[] route,
            string tokenType,
            string accessToken,
            object request)
        {
            try
            {
                var requestString = JsonConvert.SerializeObject(request);
                var content = new StringContent(requestString, Encoding.UTF8, "application/json");
                var client = new HttpClient()
                {
                    BaseAddress = new Uri(urlBase)
                };

                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue(tokenType, accessToken);

                var url = $"/{string.Join("/", route)}";

                var response = await client.PostAsync(url, content);
                var result = await response.Content.ReadAsStringAsync();

                if (!response.IsSuccessStatusCode)
                {
                    return new Response()
                    {
                        IsSuccess = false,
                        Message = result
                    };
                }

                var deserializeResponse = JsonConvert.DeserializeObject<Response>(result);
                if (!deserializeResponse.IsSuccess)
                {
                    return deserializeResponse;
                }

                if (deserializeResponse.Result == null)
                {
                    return new Response()
                    {
                        IsSuccess = true,
                        Message = deserializeResponse.Message ?? string.Empty
                    };
                }

                return new Response()
                {
                    IsSuccess = true,
                    Message = deserializeResponse.Message ?? string.Empty,
                    Result = JsonConvert.DeserializeObject<T>(deserializeResponse.Result.ToString())
                };
            }
            catch (Exception ex)
            {
                return new Response()
                {
                    IsSuccess = false,
                    Message = ex.Message
                };
            }
        }
    }
}
