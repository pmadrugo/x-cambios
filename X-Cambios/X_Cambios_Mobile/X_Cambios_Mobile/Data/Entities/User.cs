﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;
using X_Cambios_Mobile.Data.Interfaces;

namespace X_Cambios_Mobile.Data.Entities
{
    public class User : IEntity
    {
        [Key]
        public int Id { get; set; }

        public string Username { get; set; }

        public string PasswordHash { get; set; }

        public string UserId { get; set; }
    }
}
