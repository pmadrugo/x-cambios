﻿using System;
using System.Collections.Generic;
using System.Text;

namespace X_Cambios_Mobile.Data.Repository
{
    public class UnitOfWork
    {
        private XCambiosDbContext _dbContext;
        public UnitOfWork(XCambiosDbContext dbContext)
        {
            _dbContext = dbContext;
        }

        #region Repository Instances
        // Attributes
        private UserRepository _userRepository;
        private RatesRepository _ratesRepository;

        // Properties
        public UserRepository UserRepository
        {
            get
            {
                if (_userRepository == null)
                    _userRepository = new UserRepository(_dbContext);
                return _userRepository;
            }
        }

        public RatesRepository RatesRepository
        {
            get
            {
                if (_ratesRepository == null)
                    _ratesRepository = new RatesRepository(_dbContext);
                return _ratesRepository;
            }
        }
        #endregion
    }
}
