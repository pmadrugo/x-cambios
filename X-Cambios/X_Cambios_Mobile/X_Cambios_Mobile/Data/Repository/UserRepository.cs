﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using X_Cambios_Common.Models.Core;
using X_Cambios_Mobile.Data.Entities;
using X_Cambios_Mobile.Services;

namespace X_Cambios_Mobile.Data.Repository
{
    public class UserRepository : GenericRepository<User>
    {
        public UserRepository(XCambiosDbContext dbContext) : base(dbContext)
        {

        }

        public async Task<Response> AddUserAsync (string username, string password)
        {
            try
            {
                if (await UserExistsAsync(username))
                {
                    return new Response()
                    {
                        IsSuccess = true,
                        Message = "User already exists"
                    };
                }

                var newUserPassword = CryptoService.EncryptToMD5(password);
                var isUserCreated = await CreateAsync(
                    new User()
                    {
                        Username = username,
                        PasswordHash = newUserPassword
                    }
                );

                return isUserCreated;
            }
            catch (Exception ex)
            {
                return new Response()
                {
                    IsSuccess = false,
                    Message = $"Critical failure: {ex.Message}",
                    Result = ex
                };
            }
        }

        public async Task<bool> UserExistsAsync (string username)
        {
            var userList = await this.GetAll().ToListAsync();

            return userList.Any(x => x.Username == username);
        }

        public async Task<User> GetUserByNameAsync (string username)
        {
            var userList = await this.GetAll().ToListAsync();

            return userList.FirstOrDefault(x => x.Username == username);
        }

        public async Task<bool> LoginUserAsync (string username, string password)
        {
            var userList = await this.GetAll().ToListAsync();
            var passwordHash = CryptoService.EncryptToMD5(password);


            return userList.Any(x => x.Username == username &&
                                     x.PasswordHash == passwordHash);
        }
    }
}
