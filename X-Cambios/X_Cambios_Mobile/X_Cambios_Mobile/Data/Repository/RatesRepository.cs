﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using X_Cambios_Common.JSONModels;
using X_Cambios_Common.Services;
using X_Cambios_Mobile.Data.Entities;
using Xamarin.Forms;

namespace X_Cambios_Mobile.Data.Repository
{
    public class RatesRepository : GenericRepository<Rate>
    {
        private XCambiosDbContext _dbContext;
        public RatesRepository(XCambiosDbContext dbContext) : base(dbContext)
        {
            _dbContext = dbContext;
        }

        public async void ClearRates()
        {
            _dbContext.Rates.RemoveRange(_dbContext.Rates);

            await _dbContext.SaveChangesAsync();
        }

        
    }
}
