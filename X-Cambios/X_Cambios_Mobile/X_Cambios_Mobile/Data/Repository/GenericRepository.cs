﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using X_Cambios_Common.Models.Core;
using X_Cambios_Mobile.Data.Interfaces;

namespace X_Cambios_Mobile.Data.Repository
{
    public class GenericRepository<TEntity> where TEntity : class, IEntity
    {
        private DbContext _dbContext;
        public GenericRepository(DbContext dbContext)
        {
            _dbContext = dbContext;
        }

        /// <summary>
        /// Adds entity to the database
        /// </summary>
        /// <param name="entity"></param>
        /// <returns></returns>
        public async Task<Response> CreateAsync(TEntity entity)
        {
            try
            {
                await _dbContext
                    .Set<TEntity>()
                    .AddAsync(entity);

                if (await SaveAllAsync())
                {
                    return new Response()
                    {
                        IsSuccess = true,
                        Message = string.Empty,
                        Result = entity
                    };
                }

                return new Response()
                {
                    IsSuccess = false,
                    Message = $"Failed to add entity",
                    Result = entity
                };
            }
            catch (Exception ex)
            {
                return new Response()
                {
                    IsSuccess = false,
                    Message = $"Critical failure: {ex.Message}",
                    Result = ex
                };
            }
        }

        /// <summary>
        /// Updates entity in database
        /// </summary>
        /// <param name="entity"></param>
        /// <returns></returns>
        public async Task<Response> UpdateAsync(TEntity entity)
        {
            try
            {
                var existing = await GetByIdAsync(entity.Id);
                if (existing != null)
                {
                    _dbContext.Entry<TEntity>(existing).CurrentValues.SetValues(entity);
                    _dbContext.Entry<TEntity>(existing).State = EntityState.Modified;

                    if (await SaveAllAsync())
                    {
                        return new Response()
                        {
                            IsSuccess = true,
                            Message = string.Empty,
                            Result = existing
                        };
                    }

                    return new Response()
                    {
                        IsSuccess = false,
                        Message = $"Failed to update entity",
                        Result = entity
                    };
                }

                return new Response()
                {
                    IsSuccess = false,
                    Message = $"Could not find entity in database",
                    Result = entity
                };
            }
            catch (Exception ex)
            {
                return new Response()
                {
                    IsSuccess = false,
                    Message = $"Critical failure: {ex.Message}",
                    Result = ex
                };
            }
        }

        /// <summary>
        /// Deletes entity from database
        /// </summary>
        /// <param name="entity"></param>
        /// <returns></returns>
        public async Task<Response> DeleteAsync(TEntity entity)
        {
            try
            {
                var existing = await _dbContext
                    .Set<TEntity>()
                    .FirstOrDefaultAsync(x => x.Id == entity.Id);

                if (existing != null)
                {
                    _dbContext
                        .Set<TEntity>()
                        .Remove(existing);

                    if (await SaveAllAsync())
                    {
                        return new Response()
                        {
                            IsSuccess = true,
                            Message = string.Empty,
                            Result = entity
                        };
                    }

                    return new Response()
                    {
                        IsSuccess = false,
                        Message = $"Could not delete entity.",
                        Result = entity
                    };
                }

                return new Response()
                {
                    IsSuccess = false,
                    Message = $"Could not find entity",
                    Result = entity
                };
            }
            catch (Exception ex)
            {
                return new Response()
                {
                    IsSuccess = false,
                    Message = $"Critical failure: {ex.Message}",
                    Result = ex
                };
            }
        }

        /// <summary>
        /// Gets All entities (no tracking, direct db access)
        /// </summary>
        /// <returns></returns>
        public IQueryable<TEntity> GetAll()
        {
            return _dbContext
                .Set<TEntity>()
                .AsNoTracking();
        }

        /// <summary>
        /// Gets a single entity by Id, no tracking, dissasociated from the database
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public async Task<TEntity> GetByIdAsync(int id)
        {
            return (
                    await GetAll()
                        .ToListAsync()
                    )
                    .FirstOrDefault(x => x.Id == id);
        }

        /// <summary>
        /// Saves all data
        /// </summary>
        /// <returns></returns>
        private async Task<bool> SaveAllAsync()
        {
            return await _dbContext.SaveChangesAsync() > 0;
        }
    }
}
