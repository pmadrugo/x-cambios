﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using X_Cambios_Mobile.Data.Entities;

namespace X_Cambios_Mobile.Data
{
    public class XCambiosDbContext : DbContext
    {
        public bool IsStarted { get; private set; }

        public XCambiosDbContext()
        {
            IsStarted = false;
        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            base.OnConfiguring(optionsBuilder);
            
            optionsBuilder.UseSqlite($"Data Source={Environment.GetFolderPath(Environment.SpecialFolder.Personal)}/Data/xcambios.db");
        }

        public async void Startup()
        {
            await Database.EnsureCreatedAsync();

            await this.Database.ExecuteSqlCommandAsync("SELECT 'true' WHERE (1=1);");

            IsStarted = true;
        }

        public DbSet<User> Users { get; set; }

        public DbSet<Rate> Rates { get; set; }
    }
}
