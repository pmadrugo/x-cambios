﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace X_Cambios_Mobile.Data.Interfaces
{
    public interface IEntity
    {
        [Key]
        int Id { get; set; }
    }
}
