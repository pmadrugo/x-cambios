﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Security.Cryptography;

namespace X_Cambios_Mobile.Services
{
    public class CryptoService
    {
        public static string EncryptToMD5(string toEncrypt)
        {
            MD5CryptoServiceProvider md5provider = new MD5CryptoServiceProvider();
            StringBuilder hash = new StringBuilder();
            byte[] bytes = md5provider.ComputeHash(new UTF8Encoding().GetBytes(toEncrypt));

            for (int i = 0; i < bytes.Length; i++)
            {
                hash.Append(bytes[i].ToString("X2"));
            }

            return hash.ToString();
        }
    }
}
