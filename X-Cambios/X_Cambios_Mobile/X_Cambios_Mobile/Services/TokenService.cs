﻿using System;
using System.Collections.Generic;
using System.Text;
using X_Cambios_Common.Models.Core;
using X_Cambios_Common.Services;
using Xamarin.Forms;

namespace X_Cambios_Mobile.Services
{
    public static class TokenService
    {
        private static TokenRequest _tokenRequest;
        private static TokenResponse _token;

        public static void SetToken(string username, string password)
        {
            _tokenRequest = new TokenRequest()
            {
                Username = username,
                Password = password
            };
        }

        public static void SetToken(TokenRequest tokenRequest)
        {
            _tokenRequest = tokenRequest;
        }

        public static string GetToken()
        {
            if (_token.Expiration.CompareTo(DateTime.Now) >= 0)
            {
                GetTokenRemotely();
            }

            return _token.Token;
        }

        private static async void GetTokenRemotely()
        {
            APIService apiService = new APIService();

            var url = Application.Current.Resources["UrlAPI"].ToString();
            var response = await apiService.PostAsync<TokenResponse>(
                urlBase: url,
                route: new string[]
                {
                    "api",
                    "Account",
                    "Login"
                },
                request: _tokenRequest);

            if (response.IsSuccess)
            {
                var token = response.Result as TokenResponse;
                if (token != null)
                {
                    _token = token;
                }
            }
        }
    }
}
