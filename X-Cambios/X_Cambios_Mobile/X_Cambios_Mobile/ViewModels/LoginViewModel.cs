﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;
using System.Windows.Input;
using X_Cambios_Common.Models.Core;
using X_Cambios_Common.Services;
using X_Cambios_Mobile.Data.Repository;
using X_Cambios_Mobile.Services;
using X_Cambios_Mobile.Views;
using Xamarin.Forms;

namespace X_Cambios_Mobile.ViewModels
{
    public class LoginViewModel : BaseViewModel, INotifyPropertyChanged
    {
        #region Attributes
        private UnitOfWork _unitOfWork;

        private APIService _apiService;
        private NetService _netService;

        private bool _isRunning;
        private bool _isLoginEnabled;

        private string _email;
        private string _password;

        #endregion

        #region Properties 

        public bool IsRunning
        {
            get => _isRunning;
            set => SetValue(ref _isRunning, value);
        }

        public bool IsLoginEnabled
        {
            get => _isLoginEnabled;
            set => SetValue(ref _isLoginEnabled, value);
        }

        public string Email
        {
            get => _email;
            set => SetValue(ref _email, value);
        }

        public string Password
        {
            get => _password;
            set => SetValue(ref _password, value);
        }

        #endregion

        #region Commands

        public ICommand LoginCommand => new Command(
            async () =>
            {
                if (string.IsNullOrEmpty(Email))
                {
                    await Application.Current.MainPage.DisplayAlert(
                        "Erro",
                        "Por favor insira um E-Mail",
                        "Continuar");

                    return;
                }

                if (string.IsNullOrEmpty(Password))
                {
                    await Application.Current.MainPage.DisplayAlert(
                        "Erro",
                        "Por favor insira uma password",
                        "Continuar");

                    return;
                }

                IsRunning = true;
                IsLoginEnabled = false;

                var tokenRequest = new TokenRequest()
                {
                    Password = this.Password,
                    Username = this.Email
                };

                var connection = await _netService.CheckConnection();
                if (connection.IsSuccess)
                {
                    var url = Application.Current.Resources["UrlAPI"].ToString();
                    var response = await _apiService.PostAsync<TokenResponse>(
                        urlBase: url,
                        route: new string[]
                        {
                        "api",
                        "Account",
                        "Login"
                        },
                        request: tokenRequest);

                    if (!response.IsSuccess)
                    {
                        await Application.Current.MainPage.DisplayAlert(
                            "Erro",
                            "Password ou E-Mail incorrecto.",
                            "Continuar");

                        IsRunning = false;
                        IsLoginEnabled = true;

                        return;
                    }

                    await _unitOfWork.UserRepository.AddUserAsync(tokenRequest.Username, tokenRequest.Password);
                }
                else
                {
                    var userLogin = await _unitOfWork.UserRepository.LoginUserAsync(tokenRequest.Username, tokenRequest.Password);
                    if (!userLogin)
                    {
                        await Application.Current.MainPage.DisplayAlert(
                            "Erro",
                            "Password ou E-Mail incorrecto.",
                            "Continuar");

                        IsRunning = false;
                        IsLoginEnabled = true;

                        return;
                    }
                }

                TokenService.SetToken(tokenRequest);

                MainViewModel.GetInstance().RatesVM = new RatesViewModel();
                Application.Current.MainPage = new MasterPage();

                IsLoginEnabled = true;
                IsRunning = false;
            }
        );

        public ICommand CreateAccountCommand => new Command(
            () =>
            {
                MainViewModel.GetInstance().RegisterVM = new RegisterViewModel();
                Application.Current.MainPage = new RegisterPage();
            }
        );

        #endregion

        public LoginViewModel()
        {
            _apiService = new APIService();
            _netService = new NetService();

            _unitOfWork = new UnitOfWork(MainViewModel.GetInstance().LocalDatabase);


            IsLoginEnabled = true;
            IsRunning = false;
        }
    }
}
