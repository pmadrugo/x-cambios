﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;
using System.Windows.Input;
using X_Cambios_Common.Models.Core;
using X_Cambios_Common.Services;
using X_Cambios_Mobile.Data.Repository;
using X_Cambios_Mobile.Services;
using X_Cambios_Mobile.Views;
using Xamarin.Forms;

namespace X_Cambios_Mobile.ViewModels
{
    public class RegisterViewModel : BaseViewModel, INotifyPropertyChanged
    {
        #region Attributes

        private APIService _apiService;
        private NetService _netService;
        private UnitOfWork _unitOfWork;

        private bool _isRunning;
        private bool _isCreateAccountEnabled;

        private string _email;
        private string _password;

        #endregion

        #region Properties 

        public bool IsRunning
        {
            get => _isRunning;
            set => SetValue(ref _isRunning, value);
        }

        public bool IsCreateAccountEnabled
        {
            get => _isCreateAccountEnabled;
            set => SetValue(ref _isCreateAccountEnabled, value);
        }

        public string Email
        {
            get => _email;
            set => SetValue(ref _email, value);
        }

        public string Password
        {
            get => _password;
            set => SetValue(ref _password, value);
        }

        #endregion

        #region Commands

        public ICommand RegisterAccountCommand => new Command(
            async () =>
            {
                if (string.IsNullOrEmpty(Email))
                {
                    await Application.Current.MainPage.DisplayAlert(
                        "Erro",
                        "Por favor insira um E-Mail",
                        "Continuar");

                    return;
                }

                if (string.IsNullOrEmpty(Password))
                {
                    await Application.Current.MainPage.DisplayAlert(
                        "Erro",
                        "Por favor insira uma password",
                        "Continuar");

                    return;
                }

                IsRunning = true;
                IsCreateAccountEnabled = false;

                var connection = await _netService.CheckConnection();
                if (!connection.IsSuccess)
                {
                    IsCreateAccountEnabled = true;
                    IsRunning = false;

                    await Application.Current.MainPage.DisplayAlert(
                        "Erro",
                        connection.Message,
                        "Continuar");

                    return;
                }

                var request = new TokenRequest()
                {
                    Password = this.Password,
                    Username = this.Email
                };

                var url = Application.Current.Resources["UrlAPI"].ToString();
                var response = await _apiService.PostAsync<TokenResponse>(
                    urlBase: url,
                    route: new string[]
                    {
                        "api",
                        "Account",
                        "Register"
                    },
                    request: request);

                if (!response.IsSuccess)
                {
                    await Application.Current.MainPage.DisplayAlert(
                        "Erro",
                        response.Message,
                        "Continuar");

                    IsRunning = false;
                    IsCreateAccountEnabled = true;

                    return;
                }

                TokenService.SetToken(request);

                await _unitOfWork.UserRepository.AddUserAsync(request.Username, request.Password);

                MainViewModel.GetInstance().RatesVM = new RatesViewModel();

                Application.Current.MainPage = new MasterPage();

                IsRunning = false;
                IsCreateAccountEnabled = true;
            }
        );

        public ICommand BackToLoginCommand => new Command(
            () =>
            {
                MainViewModel.GetInstance().LoginVM = new LoginViewModel();
                Application.Current.MainPage = new LoginPage();
            }
        );

        #endregion

        public RegisterViewModel()
        {
            _apiService = new APIService();
            _netService = new NetService();
            _unitOfWork = new UnitOfWork(MainViewModel.GetInstance().LocalDatabase);


            IsCreateAccountEnabled = true;
        }
    }
}
