﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Text;
using System.Windows.Input;
using X_Cambios_Mobile.Data.Entities;
using X_Cambios_Mobile.Data.Repository;
using X_Cambios_Mobile.Views;
using Xamarin.Forms;

namespace X_Cambios_Mobile.ViewModels
{
    public class CurrencyConverterViewModel : BaseViewModel, INotifyPropertyChanged
    {
        private UnitOfWork _unitOfWork;
        private CurrencyConverterPage _currencyConverterPage;

        public CurrencyConverterViewModel(CurrencyConverterPage currencyConverterPage)
        {
            IsRefreshing = true;
            IsLoaded = false;

            _currencyConverterPage = currencyConverterPage;
            _unitOfWork = new UnitOfWork(MainViewModel.GetInstance().LocalDatabase);

            LoadRates();

            _currencyConverterPage.Content.BindingContext = this;
        }

        private bool _isRefreshing;
        public bool IsRefreshing
        {
            get => _isRefreshing;
            set => SetValue(ref _isRefreshing, value);
        }

        private bool _isLoaded;
        public bool IsLoaded
        {
            get => _isLoaded;
            set => SetValue(ref _isLoaded, value);
        }

        private List<Rate> _rates;
        public List<Rate> Rates
        {
            get => _rates;
            set => SetValue(ref _rates, value);
        }

        private Rate _sourceRate;
        public Rate SourceRate
        {
            get => _sourceRate;
            set => SetValue(ref _sourceRate, value);
        }

        private string _sourceRateName;
        public string SourceRateName
        {
            get => _sourceRateName;
            set => SetValue(ref _sourceRateName, value);
        }

        private string _targetRateName;
        public string TargetRateName
        {
            get => _targetRateName;
            set => SetValue(ref _targetRateName, value);
        }

        private Rate _toRate;
        public Rate ToRate
        {
            get => _toRate;
            set => SetValue(ref _toRate, value);
        }

        private string _inputValue;
        public string InputValue
        {
            get => _inputValue;
            set => SetValue(ref _inputValue, value);
        }

        private string _outputValue;
        public string OutputValue
        {
            get => _outputValue;
            set => SetValue(ref _outputValue, value);
        }

        private void LoadRates()
        {
            Rates = new List<Rate>(_unitOfWork.RatesRepository.GetAll());

            IsRefreshing = false;
            IsLoaded = true;
        }

        public ICommand SourceRateSelect => new Command(
            async () =>
            {
                MainViewModel.GetInstance().RateSelectionVM = new RateSelectionViewModel(_currencyConverterPage, Rates, true);
                await App.Navigator.PushAsync(new RateSelectionPage());
            }
        );

        public ICommand TargetRateSelect => new Command(
            async () =>
            {
                MainViewModel.GetInstance().RateSelectionVM = new RateSelectionViewModel(_currencyConverterPage, Rates, false);
                await App.Navigator.PushAsync(new RateSelectionPage());
            }
        );

        public ICommand ConvertCommand => new Command(
            async () =>
            {
                if (SourceRate == null)
                    return;

                if (ToRate == null)
                    return;

                double inputValue;
                if (!double.TryParse(InputValue, out inputValue))
                    return;

                OutputValue = ((inputValue * ToRate.TaxRate) / SourceRate.TaxRate).ToString("N5");
            }
        );

        public ICommand InvertCommand => new Command(
            async () =>
            {
                var temp = SourceRate;
                SourceRate = ToRate;
                ToRate = temp;

                var tempValue = InputValue;
                InputValue = OutputValue;
                OutputValue = tempValue;

                _currencyConverterPage.Content.FindByName<Label>("SourceRateName").Text = SourceRate.Name;
                _currencyConverterPage.Content.FindByName<Label>("TargetRateName").Text = ToRate.Name;

                _currencyConverterPage.Content.FindByName<Entry>("InputValue").Text = InputValue;
                _currencyConverterPage.Content.FindByName<Label>("OutputValue").Text = OutputValue;
            }
        );

        public void RefreshRatesFields()
        {
            OnPropertyChanged("SourceRateName");
            OnPropertyChanged("TargetRateName");
        }
    }
}
