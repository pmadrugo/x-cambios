﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using X_Cambios_Common.JSONModels;
using X_Cambios_Common.Services;
using X_Cambios_Mobile.Data.Entities;
using X_Cambios_Mobile.Data.Repository;
using Xamarin.Forms;

namespace X_Cambios_Mobile.ViewModels
{
    public class RatesViewModel : BaseViewModel, INotifyPropertyChanged
    {
        private APIService _apiService;
        private NetService _netService;
        private UnitOfWork _unitOfWork;

        public RatesViewModel()
        {
            IsRefreshing = true;
            IsLoaded = false;

            _apiService = new APIService();
            _netService = new NetService();

            _unitOfWork = new UnitOfWork(MainViewModel.GetInstance().LocalDatabase);

            LoadRates();
        }

        private string _searchText;
        public string SearchText
        {
            get => _searchText;
            set
            {
                _searchText = value;
                OnPropertyChanged("Rates");
            }
        }

        private ObservableCollection<Rate> _rates;
        public ObservableCollection<Rate>  Rates
        {
            get
            {
                if (string.IsNullOrEmpty(SearchText))
                    return _rates;

                var returnList = new List<Rate>();

                foreach (Rate rate in _rates)
                {
                    if (rate.Code.ToLower().IndexOf(SearchText.ToLower()) > -1 ||
                        rate.Name.ToLower().IndexOf(SearchText.ToLower()) > -1)
                    {
                        returnList.Add(rate);
                    }
                }

                return new ObservableCollection<Rate>(returnList);
            }
            set => SetValue(ref _rates, value);
        }

        private bool _isRefreshing;
        public bool IsRefreshing
        {
            get => _isRefreshing;
            set => SetValue(ref _isRefreshing, value);
        }

        private bool _isLoaded;
        public bool IsLoaded
        {
            get => _isLoaded;
            set => SetValue(ref _isLoaded, value);
        }

        private async Task LoadRates()
        {
            var checkConnection = await _netService.CheckConnection();
            if (checkConnection.IsSuccess)
            {
                var rates = await LoadRatesFromInternetAsync();
                Rates = new ObservableCollection<Rate>(rates);
            }
            else
            {
                var rates = await LoadRatesFromDBAsync();
                Rates = new ObservableCollection<Rate>(rates);
            }

            IsRefreshing = false;
            IsLoaded = true;
        }

        public async Task<List<Rate>> LoadRatesFromInternetAsync()
        {
            APIService apiService = new APIService();

            var response = await apiService.GetAsync<List<JSONRate>>(
                Application.Current.Resources["RatesAPI"].ToString(),
                new string[]
                {
                    "api",
                    "rates"
                },
                false
            );

            if (response.IsSuccess)
            {
                _unitOfWork.RatesRepository.ClearRates();

                foreach (var jsonRate in response.Result as List<JSONRate>)
                {
                    var isCreated = await _unitOfWork.RatesRepository.CreateAsync(
                        new Rate()
                        {
                            Code = jsonRate.Code,
                            Id = jsonRate.RateId,
                            Name = jsonRate.Name,
                            TaxRate = jsonRate.TaxRate
                        }
                    );
                }

                var isEuroCreated = await _unitOfWork.RatesRepository.CreateAsync(
                    new Rate()
                    {
                        Code = "EUR",
                        Id = 1,
                        Name = "Euro",
                        TaxRate = 1
                    }
                );
            }

            return await LoadRatesFromDBAsync();
        }

        public async Task<List<Rate>> LoadRatesFromDBAsync()
        {
            var ratesList = await _unitOfWork.RatesRepository.GetAll().OrderBy(x => x.Name).ToListAsync();

            return ratesList;
        }
    }
}
