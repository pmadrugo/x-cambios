﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Text;
using X_Cambios_Common.Models.Core;
using X_Cambios_Mobile.Data;

namespace X_Cambios_Mobile.ViewModels
{
    public class MainViewModel : BaseViewModel
    {
        // SINGLETON -- Main View Model
        private static MainViewModel _instance;
        private MainViewModel()
        {
            _instance = this;
            LoadMenus();
        }

        public static MainViewModel GetInstance()
        {
            if (_instance == null)
                _instance = new MainViewModel();
            return _instance;
        }

        public ObservableCollection<MenuItemViewModel> Menus { get; set; }
        private void LoadMenus()
        {
            this.Menus = new ObservableCollection<MenuItemViewModel>()
            {
                new MenuItemViewModel()
                {
                    Icon = "ic_info",
                    PageName = "CurrencyConverterPage",
                    Title = "Conversor"
                },
                new MenuItemViewModel()
                {
                    Title = "Separator",
                    IsSeparator = true
                },
                new MenuItemViewModel()
                {
                    Icon = "ic_info",
                    PageName = "AboutPage",
                    Title = "Sobre"
                },
                new MenuItemViewModel()
                {
                    Icon = "ic_exit_to_app",
                    PageName = "LoginPage",
                    Title = "Terminar Sessão"
                }
            };
        }

        private XCambiosDbContext _localDatabase;
        public XCambiosDbContext LocalDatabase
        {
            get
            {
                if (_localDatabase == null)
                    _localDatabase = new XCambiosDbContext();
                return _localDatabase;
            }
        }

        // Other pages ViewModels (insert here)
        public LoginViewModel LoginVM { get; set; }

        public RegisterViewModel RegisterVM { get; set; }

        public RatesViewModel RatesVM { get; set; }

        public CurrencyConverterViewModel CurrencyConverterVM { get; set; }

        public RateSelectionViewModel RateSelectionVM { get; set; }
    }
}
