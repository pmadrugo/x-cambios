﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Windows.Input;
using X_Cambios_Mobile.Data.Entities;
using X_Cambios_Mobile.Views;
using Xamarin.Forms;

namespace X_Cambios_Mobile.ViewModels
{
    public class RateSelectionViewModel : BaseViewModel, INotifyPropertyChanged
    {
        private CurrencyConverterPage _currencyConverterPage;
        // Refers to source currency?
        private bool _source;

        public RateSelectionViewModel(CurrencyConverterPage currencyConverterPage, List<Rate> ratesList, bool source)
        {
            _currencyConverterPage = currencyConverterPage;

            IsRefreshing = true;
            IsLoaded = false;

            _source = source;
            RatesList = new ObservableCollection<Rate>(ratesList.OrderBy(x => x.Name));

            IsRefreshing = false;
            IsLoaded = true;
        }

        private bool _isRefreshing;
        public bool IsRefreshing
        {
            get => _isRefreshing;
            set => SetValue(ref _isRefreshing, value);
        }

        private bool _isLoaded;
        public bool IsLoaded
        {
            get => _isLoaded;
            set => SetValue(ref _isLoaded, value);
        }

        private ObservableCollection<Rate> _ratesList;
        public ObservableCollection<Rate> RatesList
        {
            get {
                if (string.IsNullOrEmpty(_searchText))
                    return _ratesList;

                var returnList = new List<Rate>();
                foreach (var rate in _ratesList)
                {
                    if (rate.Code.ToLower().IndexOf(SearchText.ToLower()) > -1 ||
                        rate.Name.ToLower().IndexOf(SearchText.ToLower()) > -1)
                    {
                        returnList.Add(rate);
                    }
                }

                return new ObservableCollection<Rate>(returnList);
            }
            set => SetValue(ref _ratesList, value);
        }

        private string _searchText;
        public string SearchText
        {
            get => _searchText;
            set
            {
                _searchText = value;
                OnPropertyChanged("RatesList");
            }
        }

        public ICommand ChooseRate => new Command<Rate>(
            async (rate) =>
            {
                if (_source)
                {
                    MainViewModel.GetInstance().CurrencyConverterVM.SourceRate = rate;
                    MainViewModel.GetInstance().CurrencyConverterVM.SourceRateName = rate.Name;

                    _currencyConverterPage.Content.FindByName<Label>("SourceRateName").Text = rate.Name;
                }
                else
                {
                    MainViewModel.GetInstance().CurrencyConverterVM.ToRate = rate;
                    MainViewModel.GetInstance().CurrencyConverterVM.TargetRateName = rate.Name;

                    _currencyConverterPage.Content.FindByName<Label>("TargetRateName").Text = rate.Name;
                }
                await App.Navigator.PopAsync();
            }
        );
    }
}
