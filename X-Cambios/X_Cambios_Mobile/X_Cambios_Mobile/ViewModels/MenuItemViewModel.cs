﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Input;
using X_Cambios_Mobile.Models;
using X_Cambios_Mobile.Views;
using Xamarin.Forms;

namespace X_Cambios_Mobile.ViewModels
{
    public class MenuItemViewModel : Models.MenuItem
    {
        public bool IsSeparator { get; set; }

        public bool IsNotSeparator { get => !IsSeparator; }

        public ICommand SelectMenuCommand => new Command(
            async () =>
            {
                App.Master.IsPresented = false;

                switch (PageName)
                {
                    case "CurrencyConverterPage":
                    {
                        var currencyConverterPage = new CurrencyConverterPage();
                        MainViewModel.GetInstance().CurrencyConverterVM = new CurrencyConverterViewModel(currencyConverterPage);
                        await App.Navigator.PushAsync(currencyConverterPage);
                    } break;

                    case "AboutPage":
                        await App.Navigator.PushAsync(new AboutPage());
                    break;

                    default:
                    {
                        if (string.IsNullOrEmpty(PageName))
                            return;

                        MainViewModel.GetInstance().LoginVM = new LoginViewModel();
                        Application.Current.MainPage = new LoginPage();
                    } break;
                }
            }
        );
    }
}
