﻿using System;
using System.Collections.Generic;
using System.Text;
using X_Cambios_Mobile.ViewModels;

namespace X_Cambios_Mobile.Infrastructure
{
    public class InstanceLocator
    {
        public MainViewModel Main { get; set; }

        public InstanceLocator()
        {
            this.Main = MainViewModel.GetInstance();
        }
    }
}
