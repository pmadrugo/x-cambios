﻿using System;
using System.Collections.Generic;
using System.Text;

namespace X_Cambios_Mobile.Models
{
    public class MenuItem
    {
        public string Icon { get; set; }

        public string PageName { get; set; }

        public string Title { get; set; }
    }
}
