﻿using System;
using X_Cambios_Mobile.ViewModels;
using X_Cambios_Mobile.Views;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace X_Cambios_Mobile
{
    public partial class App : Application
    {
        public static MasterPage Master { get; internal set; }

        public static NavigationPage Navigator { get; internal set; }

        public App()
        {
            InitializeComponent();

            MainViewModel.GetInstance().LoginVM = new LoginViewModel();
            MainViewModel.GetInstance().RegisterVM = new RegisterViewModel();

            MainPage = new LoginPage();
        }

        protected override void OnStart()
        {
            // Handle when your app starts
        }

        protected override void OnSleep()
        {
            // Handle when your app sleeps
        }

        protected override void OnResume()
        {
            // Handle when your app resumes
        }
    }
}
