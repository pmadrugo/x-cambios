﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Android.App;
using Android.Content;
using Android.Content.PM;
using Android.OS;
using Android.Runtime;
using Android.Support.V7.App;
using Android.Util;
using Android.Views;
using Android.Widget;
using X_Cambios_Mobile.ViewModels;

namespace X_Cambios_Mobile.Droid
{
    [Activity(Theme = "@style/SplashScreen.Splash", MainLauncher = true, NoHistory = true)]
    public class SplashActivity : AppCompatActivity
    {
        static readonly string TAG = "X:" + typeof(SplashActivity).Name;

        public override void OnCreate(Bundle savedInstanceState, PersistableBundle persistentState)
        {
            RequestedOrientation = ScreenOrientation.Portrait;

            base.OnCreate(savedInstanceState, persistentState);

            Log.Debug(TAG, "Splash Screen Loaded");
        }

        protected override void OnResume()
        {
            base.OnResume();

            Task startupWork = new Task(() => { StartupXCambios(); });

            startupWork.Start();
        }

        async void StartupXCambios()
        {
            // Do stuff here
            if (!Directory.Exists($"{System.Environment.GetFolderPath(System.Environment.SpecialFolder.Personal)}/Data/"))
            {
                Directory.CreateDirectory($"{System.Environment.GetFolderPath(System.Environment.SpecialFolder.Personal)}/Data/");
            }

            MainViewModel.GetInstance().LocalDatabase.Startup();

            StartActivity(new Intent(Application.Context, typeof(MainActivity)));
        }

    }
}