﻿using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Tokens;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using X_Cambios_Common.Models.Core;
using X_Cambios_Web.Helper.Interfaces;
using X_Cambios_Web.Models;

namespace X_Cambios_Web.ApiControllers
{
    [Route("api/[controller]/[action]")]
    public class AccountController : Controller
    {
        private readonly IConfiguration _configuration;
        private readonly IUserHelper _userHelper;
        public AccountController(IUserHelper userHelper, IConfiguration configuration)
        {
            _userHelper = userHelper;
            _configuration = configuration;
        }

        private dynamic CreateToken(string userEmail)
        {
            var claims = new[]
            {
                new Claim(JwtRegisteredClaimNames.Sub, userEmail),
                new Claim(JwtRegisteredClaimNames.Jti, Guid.NewGuid().ToString())
            };

            var key = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(_configuration["Tokens:Key"]));
            var credentials = new SigningCredentials(key, SecurityAlgorithms.HmacSha256);

            var token = new JwtSecurityToken(
                _configuration["Tokens:Issuer"],
                _configuration["Tokens:Audience"],
                claims,
                expires: DateTime.UtcNow.AddDays(15),
                signingCredentials: credentials);

            return new TokenResponse()
            {
                Token = new JwtSecurityTokenHandler().WriteToken(token),
                Expiration = token.ValidTo
            };
        }

        [HttpPost]
        public async Task<IActionResult> Login([FromBody] LoginViewModel model)
        {
            if (ModelState.IsValid)
            {
                var user = await _userHelper.GetUserByEmailAsync(model.Username);
                if (user != null)
                {
                    var isPasswordValidated = await _userHelper.ValidatePasswordAsync(user, model.Password);
                    if (isPasswordValidated.Succeeded)
                    {
                        return Ok(
                            new Response()
                            {
                                IsSuccess = true,
                                Result = CreateToken(model.Username)
                            }
                        );
                    }

                    return Ok(
                        new Response()
                        {
                            IsSuccess = false,
                            Message = "Failed Login"
                        }
                    );
                }

                return Ok(
                    new Response()
                    {
                        IsSuccess = false,
                        Message = "User doesn't exist"
                    }
                );
            }

            return BadRequest();
        }

        [HttpPost]
        public async Task<IActionResult> Register([FromBody] RegisterViewModel model)
        {
            if (ModelState.IsValid)
            {
                var userExists = await _userHelper.GetUserByEmailAsync(model.Username);
                if (userExists != null)
                    return Ok(
                        new Response()
                        {
                            IsSuccess = false,
                            Message = "Email already registered"
                        }
                    );

                var isUserCreated = await _userHelper.AddUserAsync(
                    new Data.Entities.User()
                    {
                        UserName = model.Username,
                        Email = model.Username
                    },
                    model.Password);

                if (!isUserCreated.Succeeded)
                {
                    return Ok(
                        new Response()
                        {
                            IsSuccess = false,
                            Message = "Failed to create user"
                        }
                    );
                }

                return Ok(
                    new Response()
                    {
                        IsSuccess = true,
                        Message = "User created, logging in",
                        Result = CreateToken(model.Username)
                    }
                );
            }

            return BadRequest();
        }

        //[HttpPost]
        //public async Task<IActionResult> ForgotPassword([FromBody] ForgotPasswordViewModel model)
        //{
        //    // TODO - Forgot Password
        //}

        //[HttpPost]
        //[Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        //public async Task<IActionResult> ChangePassword([FromBody] ChangePasswordViewModel model)
        //{
        //    // TODO - Change Password
        //}
    }
}
