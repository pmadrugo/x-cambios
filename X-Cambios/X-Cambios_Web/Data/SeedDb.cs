﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace X_Cambios_Web.Data
{
    public class SeedDb
    {
        private readonly XCambiosDbContext _dbContext;

        public SeedDb(XCambiosDbContext dbContext)
        {
            _dbContext = dbContext;
        }

        public async Task SeedAsync()
        {
            await this._dbContext.Database.EnsureCreatedAsync();
        }
    }
}
