﻿using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using X_Cambios_Web.Data.Entities;

namespace X_Cambios_Web.Data
{
    public class XCambiosDbContext : IdentityDbContext<User>
    {
        public XCambiosDbContext(DbContextOptions<XCambiosDbContext> options) : base(options) { }
    }
}
