﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace X_Cambios_Web.Models
{
    public class RegisterViewModel
    {
        public string Username { get; set; }

        public string Password { get; set; }
    }
}
