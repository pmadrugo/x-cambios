﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace X_Cambios_Web.Models
{
    public class ForgotPasswordViewModel
    {
        public string Username { get; set; }
    }
}
