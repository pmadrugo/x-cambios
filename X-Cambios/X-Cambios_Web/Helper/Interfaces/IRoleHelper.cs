﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace X_Cambios_Web.Helper.Interfaces
{
    public interface IRoleHelper
    {
        Task<Microsoft.AspNetCore.Identity.IdentityResult> CreateAsync(Microsoft.AspNetCore.Identity.IdentityRole role);
        Task<Microsoft.AspNetCore.Identity.IdentityResult> DeleteAsync(Microsoft.AspNetCore.Identity.IdentityRole role);
        IQueryable<Microsoft.AspNetCore.Identity.IdentityRole> GetAllRoles();
        Task<bool> RoleExistsAsync(string roleName);
    }
}
