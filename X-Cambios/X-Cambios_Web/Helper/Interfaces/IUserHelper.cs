﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace X_Cambios_Web.Helper.Interfaces
{
    public interface IUserHelper
    {
        Task<Microsoft.AspNetCore.Identity.IdentityResult> AddToRoleAsync(Data.Entities.User user, string roleName);
        Task<Microsoft.AspNetCore.Identity.IdentityResult> AddUserAsync(Data.Entities.User user, string password);
        Task<bool> CanSignInAsync(Data.Entities.User user);
        Task<Microsoft.AspNetCore.Identity.IdentityResult> ChangePasswordAsync(Data.Entities.User user, string oldPassword, string newPassword);
        Task<Data.Entities.User> GetUserByEmailAsync(string email);
        Task<Data.Entities.User> GetUserByIdAsync(string userId);
        Task<Microsoft.AspNetCore.Identity.SignInResult> LoginAsync(Models.LoginViewModel model);
        Task LogoutAsync();
        Task<Microsoft.AspNetCore.Identity.IdentityResult> UpdateUserAsync(Data.Entities.User user);
        Task<Microsoft.AspNetCore.Identity.SignInResult> ValidatePasswordAsync(Data.Entities.User user, string password);
    }
}
